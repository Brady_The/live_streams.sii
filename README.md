# Additional information
**102** radio stations were edited in total.  
028 of those included [renamings](#renamed) where it seemed appropriate.  
007 of those were [replacements](#replaced), usually by their successors.  
014 of those were [removals](#removed). 3 of which were lower quality duplicates. 11 had no identifiable replacements.  
008 of those are working in the web browser, but are [not working](#not-working-in-game) in the game. Those were left in the file in case of a possible support in the future.

## Installation
1. Open the folder 'Euro Truck Simulator 2' in your documents (Default: C:\Users\%USERNAME%\Documents)
2. Download <a href="https://gitlab.com/Brady_The/live_streams.sii/-/raw/main/live_streams.sii" target="_blank">live_stream.sii.
3. Replace the existing 'live_stream.sii' with the one you just downloaded.
```
I recommend not using the 'Update from Internet' button in game.
I recommend either keeping a copy of 'live_stream.sii' or the download link close by.
```

# Radio Stations

## <i class="fa-solid fa-pen" aria-hidden="true"></i> Renamed
Dimensione Suono Due -> Dimensione Suono Soft  
FRED FILM RADIO -> FRED FILM RADIO Latvian  
Jack FM Oxford -> JACKfm Oxfordshire  
Jemné melódie -> Rádio Melody  
Nation Radio Cardiff -> Nation Radio Wales  
NRJ Norway -> NRJ Norge  
Q Music Amsterdam -> Qmusic Netherlands  
Q-Music -> Qmusic Belgium  
Raadio Uuno -> MyHits (https://en.wikipedia.org/wiki/Raadio_Uuno)  
Radio 538 Top 40 -> Radio 538 Top 50  
Radio Country -> P10 Country  
Radio Minimal mix -> Minimal Mix Radio  
Radio Noord Groningen -> RTV Noord  
Radio-1 -> Radio 1 Norge  
RadioJAZZ FM -> RadioJAZZ.fm  
Retro FM -> Retro FM Estonia  
RFM -> RFM 70's  
RTL 2 Calais -> RTL 2  
SkyPlus -> Sky Plus  
Slam! Boom Room -> SLAM! The Boom Room  
Solar Sound System -> Radio3S - SolarSoundSystem  
Star Cambridge -> Star Radio  
Swansea Sound -> Greatest Hits Radio South Wales (https://en.wikipedia.org/wiki/Greatest_Hits_Radio_South_Wales)  
The Arrow -> Arrow Rock Radio  
Veronica Rock -> Veronica Rockradio  
Veronica Top 1000 -> Veronica Top 1000 Allertijden  
Virage Lyon -> Virage Radio - Lyon  
Virgin Radio -> Virgin Radio Officiel

## <i class="fa-solid fa-rotate-right" aria-hidden="true"></i> Replaced
Radio Gothic -> Atma FM - Channel 2  
LeineHertz Hannover -> leineradio Hannover (http://www.leinehertz.net/)  
Rock n Pop Hamburg -> ROCK ANTENNE Hamburg  
France Bleu -> France Bleu Paris  
Radio Aligator Bratislava -> Bratislavské rádio (https://medialne.trend.sk/radia/radio-aligator-meni-vlastnika-aprila-ma-nastupcu-novym-formatom, https://www.radia.sk/player/the-end)  
Veronica Hit Radio -> Arrow Caz! (https://en.wikipedia.org/wiki/HitRadio_Veronica)

## <i class="fa-solid fa-eraser" aria-hidden="true"></i> Removed
Dubplate.fm - Drum & Bass -> Lower quality duplicate  
Dubplate.fm - Dubstep -> Lower quality duplicate  
Dubplate.fm - Urban Electro -> Lower quality duplicate  
DR P7 Mix -> Dead (https://en.wikipedia.org/wiki/DR_P7_Mix)  
Happenstance Radio -> Appears to be dead (http://www.haprad.com/)  
Netiraadio - Teistsugne -> Dead (https://www.netiraadio.ee/)  
NERadio House & Trance -> Dead (http://www.neradio.com/)  
Radio Wakai -> Appears to be dead  
Dental Radio -> Appears to be dead, http://www.dentalradio.pl/  
Rádio Šport -> Appears to be dead  
Anténa Hit Rádio -> Appears to be dead (https://www.radia.sk/player/antena-hitradio)  
Rádio KISS -> Appears to be dead (http://www.kissradio.sk/)  
Rádio ONE -> Appears to be dead (https://www.radia.sk/player/one)  
Szczecinie Radio -> Appears to be dead (http://www.radio98i9.pl/)

## <i class="fa-solid fa-link-slash" aria-hidden="true"></i> Not working In-Game
Felixstowe Radio  
Mix FM 102.7  
Nostalgie BE  
Nostalgie Rock  
Pirate Rock  
Radio Kolor  
RadioJAZZ.fm  
Retro FM Latvija  
```
Any of the radio stations on this list have been labbeled with '[¦]'.
```
#### Reddit thread
https://old.reddit.com/r/trucksim/comments/vp2985/updated_live_streamssii_2022/
